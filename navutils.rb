
require 'rake'
require 'fileutils'

module Nav

  def Nav.url htmltask

    pathnames = htmltask.name.pathmap("%X").split("/")

    ignore_last = htmltask.name.pathmap("%n") == "index" ? 2 : 1

    links = pathnames.take(pathnames.length-ignore_last).map.with_index{ |x,i|
      if i == 0 then
        "link:" + "../" * (pathnames.size-i-1)  + x + "/index.html[home]"
      else
        "link:" + "../" * (pathnames.size-i-1)  + x + "/index.html[#{pathnames[i]}]"
      end
    }

    toc = "link:" + "../" * (pathnames.length-1) + "html/toc.html[TOC]"

    unless links == []
      links << pathnames[-ignore_last]
    else
      links << "link:../html/index.html[home]"
    end

    "#{toc} (you are here: " + links.join("/") + ")"
  end


  def Nav.docroot htmltask
    pathnames = htmltask.name.pathmap("%X").split("/")
    "../" * (pathnames.size() - 2)
  end

end



module Toc

  def Toc.regenerate marshal, adoc, toc

    haveToc = File.file? marshal

    if haveToc
      oldToc = File.open(marshal, "r"){|m| Marshal.load(m)}
      haveToc = oldToc == toc

    end

    unless haveToc

      hirachy = -> (fa) {
        fa.inject({}) {|h,i|
          t = h
          i.split("/").each {|n|
            t[n] ||= {}
            t = t[n]
          }
          h
        }
      }


      out = -> (h, p, io) {
         path = p.empty? ? "." : p.join("/")
         io.puts("#{"*" * p.length} link:./#{path}/index.html[#{p[-1].tr '-', ' '}]") unless p==[]
         h.select{ |k,v| v == {}}.keys.sort.each { |k|
           basename = k.pathmap"%n"
           link = "link:./#{path}/#{basename}.html[#{basename.tr '-', ' '}]"
           io.puts("#{"*" * (p.length+1)} #{link}") unless k =="index.adoc"
         }

         h.select{ |k,v| v != {}}.sort.each { |k,v|
           out.call(v, p + [k], io) unless h.empty?
         }
      }

      File.open(adoc, 'w') { |f|


        f.puts "= The documentation index"
        f.puts "This index is sorted alphabetically since this works without additional work."
        f.puts "\n"

        f.puts("")
        f.puts("== Table Of Content")
        f.puts("")
        f.puts("* link:./index.html[home]")

        out.call hirachy.call(toc - [adoc.pathmap("%f"), "index.adoc"]), [], f
      }

      #puts "New TOC written, see #{adoc}"

      File.open(marshal, "w"){|m| m.write(Marshal.dump(toc.to_a))} unless haveToc

     end

  end


  # produce asciidoc like index
  def Toc.regenerate2 marshal, adoc, toc

    haveToc = File.file? marshal

    if haveToc
      oldToc = File.open(marshal, "r"){|m| Marshal.load(m)}
      haveToc = oldToc == toc
    end

    unless haveToc

      hirachy = -> (fa) {
        fa.inject({}) {|h,i|
          t = h
          i.split("/").each {|n|
            t[n] ||= {}
            t = t[n]
          }
          h
        }
      }

      out = -> (h, p, io) do

         path = p.empty? ? "." : p.join("/")

         io.puts(%q{<li><a href="} + "./#{path}/index.html" + %q{">} + "#{p[-1].tr '-', ' '}</a>") unless p==[]

         # handle all files in the dir as long as they are not index.html
         h.select{ |k,v| v == {}}.keys.sort.each { |k|
           unless k =="index.adoc"
             basename = k.pathmap"%n"
             link = "link:./#{path}/#{basename}.html[#{basename.tr '_', ' '}]"
             io.puts %q{<ul class="sectlevel} + "#{p.length + 1}" + %q{">} unless p.length == 0
             io.puts %q{<li><a href="} + "./#{path}/#{basename}.html"  + %q{">} + "#{basename.tr '_', ' '}</a></li>"
             io.puts '</ul>' unless p.length == 0
           end
         }

         # go over all sub directories
         h.select{ |k,v| v != {}}.sort.each { |k,v|
           io.puts(%q{<ul class="sectlevel} + "#{p.length + 1}" + %q{">}) unless p==[]
           out.call(v, p + [k], io) unless h.empty?
           io.puts ('</ul>') unless p==[]
         }

         io.puts('</li>') unless p==[]
      end

      File.open(adoc, 'w') do |f|

        f.puts "= The documentation index"
        f.puts "This index is sorted alphabetically since this works without additional work."
        f.puts "\n"

        f.puts '++++'
        f.puts '<div id="toc" class="toc">'
        f.puts '<div id="toctitle" class="title">Table of Contents</div>'

        f.puts %q{<ul class="sectlevel1">}
        f.puts %q{<li><a href="./index.html">home</a>}
        f.puts '</li>'
        #f.puts '</ul>'

        #f.puts %q{<ul class="sectlevel1">}
        out.call hirachy.call(toc - [adoc.pathmap("%f"), "index.adoc"]), [], f

        f.puts '</ul>'

        f.puts '</div>'
        f.puts '++++'

      end

      #puts "New TOC written, see #{adoc}"

      File.open(marshal, "w"){|m| m.write(Marshal.dump(toc.to_a))} unless haveToc

    end

  end


end







