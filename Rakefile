# coding: utf-8

require 'rake'
require 'fileutils'
require 'asciidoctor'
require 'asciidoctor/cli'
require 'webrick'
require 'guard'
require 'guard/commander' # needed because of https://github.com/guard/guard/issues/793

require_relative 'navutils'

BASEDIR  =  File.dirname __FILE__
PACKNAME  = File.basename  BASEDIR
HTMLOUT_PREFIX = "html"
SRCDIR_PREFIX = "src"
HTMLOUT   = File.join(BASEDIR, HTMLOUT_PREFIX)
SRCDIR = File.join(BASEDIR, SRCDIR_PREFIX)

# PACKPATH  = File.basename __FILE__  + "/" + HTMLOUT

#TODO serve shall take parameter, if localhost or * will be used


# all - except - plus toc if its not there
ADOCS = Rake::FileList["src/**/*.adoc", "src/**/*.md"] | ["src/toc.adoc"] 

# whatever.*.adoc are include files,
def is_include_doc? (docname)
  docname.pathmap("%X").pathmap("%x") != "" 
end
DOCS = ADOCS.reject { |adoc|  is_include_doc? adoc }
INCLS = ADOCS.select { |adoc|  is_include_doc? adoc }

ALLDIRS = Rake::FileList["src/**/*"].select {|d| File.directory? d}


ALLDIRS.each {|d| directory File.join(HTMLOUT_PREFIX, d) }

Toc.regenerate2 "#{SRCDIR}/toc.marshal", "#{SRCDIR}/toc.adoc" , DOCS

HTMLDEPS=Hash[ DOCS.collect do |doc|
                 basename = doc.pathmap "%X"  
                 task = basename.delete_prefix "#{SRCDIR_PREFIX}/"                 
                 includes = INCLS.select {|inc| inc.start_with? basename + "."}
                  # if something changes in resources, rebuild
                 includes.concat Rake::FileList["#{basename}.resources/*.*"]
                 # if something changes in images, rebuild
                 includes.concat Rake::FileList["#{basename}.images/*.*"]
                 [ "#{HTMLOUT_PREFIX}/#{task}.html" , [doc] + includes ]
               end
             ] 
 

TASKS = Hash[HTMLDEPS.keys.collect { |x| 
  [(x.sub("html/", "src/").pathmap("%X")).to_sym, x]
}]  


#ensure all directories exists 
# and directory contents for a task are copied
TASKS.each do |taskname, html|

  # get the dirs with files to copy 
  dirs = ALLDIRS.select{|d| 
    ["#{taskname}.images","#{taskname}.data"].include? d  
  }
  out_dirs = dirs.map {|d| d.sub("src/", "html/") }
  out_dirs.each { |d| 
    directory d
  }

  # create the copy targets
  resources = []
  dirs.each { |d| 
    resources.concat Rake::FileList["#{d}/*"]  
  }
  files = Hash[resources.map {|r| [r, r.sub("src/", "html/")]}]

  files.each do |src, dest|
    # need hanling copy of html files extra since html has a rule 
    # html files are usually generated, but for the case on shall be linked ...
    if src.pathmap('%x') == '.html'
      task dest do
        unless FileUtils.uptodate?(dest, [src])
          FileUtils.mkdir_p File.dirname(dest)
          cp src, dest, verbose: true
        end
      end
    else
      file dest => src do |task|
        unless FileUtils.uptodate?(task.name, [task.prerequisites.first])
          cp task.prerequisites.first, task.name, verbose: true 
        end
      end
    end
  end  

  filedeps = files.values

                    # note, order might matter
  task taskname =>  [:outdir] + out_dirs + [html] + filedeps  do
    
  end
end


multitask :default => TASKS.keys

desc 'create html out directory'
task :outdir do
 FileUtils.mkdir_p HTMLOUT if not File.exists? HTMLOUT 
end 


desc 'run bundler to install/setup wanted gems'
task :setup do
  if File.exist? 'Gemfile'
    system 'bundle install'
  else
    puts "not possible, no Gemfile found, bad, something is very wrong!"
    exit 1
  end
  # no more tasks, need to reset env
  exit 0
end

ATTRIBS=["icons=font",  "linkcss", "source-highlighter=prettify@"]

rule ".html" => ->(html){HTMLDEPS[html]} do |html|

  mkdir_p html.name.pathmap "%d"
  


  docattribs =[]
  # docattribs << ["navurl='#{Nav.url(html)}'"]
  # docattribs <<  "docroot='/html'"
  base_name = html.name.pathmap "%n"
  images_dir_name = base_name  + ".images"
  resources_dir_name = base_name  + ".resources"
  data_dir_name = base_name  + ".data"

  docattribs << "imagesdir@=#{images_dir_name}" #if File.directory? images_dir_name
  docattribs << "resourcesdir@=#{resources_dir_name}" #if File.directory? images_dir_name
  docattribs << "datadir@=#{data_dir_name}" #if File.directory? images_dir_name

  attribs =(ATTRIBS + docattribs).map{ |a| "-a #{a}" }.join " "
    
  options  = '-r asciidoctor-diagram '
  options << attribs
  #options << attribs.join(' ').to_s unless attribs.empty?
  options << " -o #{html.name} #{html.source}"

  puts "asciidoctor #{options}"

  invoker = Asciidoctor::Cli::Invoker.new options.split(' ')
  GC.start
  invoker.invoke!

  raise "generator return: #{invoker.code}" unless invoker.code.zero?
  
end


task :help do

puts %{
 You can run rake -AT to see all tasks, 
 but this is not what you want to do.

 run:
  rake 
    to build the html documentation

  rake clean 
    to remove existing html documentation

  rake setup
    to run bundler and install/setup wanted gems  

  rake help
    to see this text 

  rake serve
    start guard and a webserver on localhost:9099  

}
  
end

desc 'remove exising html docuementation'
task :clean  do
  FileUtils.rm_rf [HTMLOUT, "#{BASEDIR}/toc.marshal", "#{BASEDIR}/toc.adoc"]
 
end


desc 'run WEBrick to serve generated pages and Guard for auto generation'
task :serve do
  # pid = fork do
  fork do
    server = WEBrick::HTTPServer.new(DocumentRoot: './html',
                                     BindAddress: '127.0.0.1',
                                     Port: 9099)

    %w[INT TERM].each do |signal|
      Signal.trap(signal) { server.shutdown }
    end

    server.start
    puts 'WEBrick::shutdown via signal'
  end

  Guard.start no_interactions: true
end